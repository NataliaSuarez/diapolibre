# diapolibre

## diapositivas internas de Cambá

#### Desarrollo - Script para correr la diapo

##### Levantar un servidor sin refresco automático (hot-reload)
```bash
# Usando python 2
python -m SimpleHTTPServer 8000
# Usando python 3
python3 -m http.server 8000 --bind 0.0.0.0
```

##### Levantar un servidor con refresco automático (hot-reload)

*Usando npx (sin instalar la/s dependencia/s)*
```bash
# En el root del proyecto (dónde está index.html)
npx browser-sync start -s -f . --no-notify --host 0.0.0.0 --port 8000
```

*Usando npm (instalando la/s dependencia/s, es más rápido)*
```bash
# Instalar browser-sync con npm
npm i -g browser-sync
# En el root del proyecto (dónde está index.html)
browser-sync start -s -f . --no-notify --host 0.0.0.0 --port 8000
```

##### Visualizar la web

*Averiguar la Lan IP actual*
```bash
# Ubuntu
ifconfig
# Debian
ip -o -br -c a
```

Apuntar el navegador o dispositivo móvil a la IP Lan actual. Por ejemplo: 192.168.0.112:8000
